#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "kfb.h"
#include "pgm.h"

int main(int argc, char **argv) {
  int owidth;
  int oheight;
  if (argc > 2) {
    owidth = atoi(argv[1]);
    oheight = atoi(argv[2]);
  } else {
    return 1;
  }
  if (! (owidth > 0 && oheight > 0)) {
    return 1;
  }
  kfb *in = kfb_load(stdin);
  if (! in) { return 1; }
  int iwidth = kfb_width(in);
  int iheight = kfb_height(in);
  kfb *out = kfb_alloc(in, owidth, oheight);
  if (! out) { kfb_free(in); return 1; }
  double sx = iwidth / (double) owidth;
  double sy = iheight / (double) oheight;
  #pragma omp parallel for
  for (int i = 0; i < owidth; ++i) {
    for (int j = 0; j < oheight; ++j) {
      kfb_set(out, i, j, kfb_get_cubic(in, sx * i, sy * j));
    }
  }
  kfb_save(out, stdout);
  kfb_free(out);
  kfb_free(in);
  return 0;
}
