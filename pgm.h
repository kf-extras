#ifndef PGM_H
#define PGM_H 1

#include <stdio.h>

struct pgm;
typedef struct pgm pgm;

extern pgm *pgm_alloc(int width, int height);
extern void pgm_free(pgm *pgm);
extern int pgm_width(const pgm *pgm);
extern int pgm_height(const pgm *pgm);
extern void pgm_set(pgm *pgm, int i, int j, int g);
extern void pgm_setf(pgm *pgm, int i, int j, float g);
extern int pgm_save(const pgm *pgm, FILE *out);

#endif
