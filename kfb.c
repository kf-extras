#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "kfb.h"

typedef struct colour { unsigned char r, g, b; } colour;

struct kfb {
  size_t width;
  size_t height;
  int *counts;
  int iterdiv;
  int parts;
  colour *keys;
  int maxiter;
  float *trans;
  int hasde;
  float *de;
};

extern void kfb_free(kfb *k) {
  if (k) {
    if (k->trans) { free(k->trans); k->trans = 0; }
    if (k->keys) { free(k->keys); k->keys = 0; }
    if (k->counts) { free(k->counts); k->counts = 0; }
    if (k->de) { free(k->de); k->de = 0; }
    free(k);
  }
}

extern kfb *kfb_alloc(const kfb *proto, size_t width, size_t height) {
  assert(proto);
  assert(proto->keys);
  kfb *k = (kfb *) calloc(1, sizeof(struct kfb));
  if (! k) { return 0; }
  k->width = width;
  k->height = height;
  k->counts = (int *) calloc(1, width * height * sizeof(int));
  if (! k->counts) { goto fail; }
  k->trans = (float *) calloc(1, width * height * sizeof(float));
  if (! k->trans) { goto fail; }
  k->iterdiv = proto->iterdiv;
  k->parts = proto->parts;
  k->keys = (struct colour *) malloc(k->parts * sizeof(struct colour));
  if (! k->keys) { goto fail; }
  memcpy(k->keys, proto->keys, k->parts * sizeof(struct colour));
  k->maxiter = proto->maxiter;
  k->de = (float *) calloc(1, width * height * sizeof(float));
  return k;
fail:
  kfb_free(k);
  return 0;
}

extern kfb *kfb_load(FILE *in) {
  // FIXME check little-endian host, or convert
  assert(sizeof(int) == 4);
  assert(sizeof(float) == 4);
  assert(in);
  int mk = getc(in);
  int mf = getc(in);
  int mb = getc(in);
  if (mk != 'K' || mf != 'F' || mb != 'B') { return 0; }
  kfb *k = (kfb *) calloc(1, sizeof(struct kfb));
  if (! k) { return 0; }
  int w = 0;
  int h = 0;
  if (1 != fread(&w, sizeof(int), 1, in)) { goto fail; }
  k->width = w;
  if (1 != fread(&h, sizeof(int), 1, in)) { goto fail; }
  k->height = h;
  if (! (k->counts = (int *) malloc(k->width * k->height * sizeof(int)))) { goto fail; }
  if (1 != fread(k->counts, k->width * k->height * sizeof(int), 1, in)) { goto fail; }
  if (1 != fread(&k->iterdiv, sizeof(int), 1, in)) { goto fail; }
  if (1 != fread(&k->parts, sizeof(int), 1, in)) { goto fail; }
  if (! (k->keys = (struct colour *) malloc(k->parts * sizeof(struct colour)))) { goto fail; }
  if (1 != fread(k->keys, k->parts * sizeof(struct colour), 1, in)) { goto fail; }
  if (1 != fread(&k->maxiter, sizeof(int), 1, in)) { goto fail; }
  if (! (k->trans = (float *) malloc(k->width * k->height * sizeof(float)))) { goto fail; }
  if (1 != fread(k->trans, k->width * k->height * sizeof(float), 1, in)) { goto fail; }
  if (! (k->de = (float *) malloc(k->width * k->height * sizeof(float)))) { goto fail; }
  if (1 != fread(k->de, k->width * k->height * sizeof(float), 1, in)) { k->hasde = 0; } else { k->hasde = 1; }
  return k;
fail:
  kfb_free(k);
  return 0;
}

extern int kfb_save(const kfb *kfb, FILE *out) {
  // FIXME check little-endian host, or convert
  assert(sizeof(int) == 4);
  assert(sizeof(float) == 4);
  assert(out);
  assert(kfb);
  assert(kfb->counts);
  assert(kfb->keys);
  assert(kfb->trans);
  putc('K', out);
  putc('F', out);
  putc('B', out);
  int w = kfb->width;
  int h = kfb->height;
  fwrite(&w, sizeof(int), 1, out);
  fwrite(&h, sizeof(int), 1, out);
  fwrite(kfb->counts, kfb->width * kfb->height * sizeof(int), 1, out);
  fwrite(&kfb->iterdiv, sizeof(int), 1, out);
  fwrite(&kfb->parts, sizeof(int), 1, out);
  fwrite(kfb->keys, kfb->parts * sizeof(colour), 1, out);
  fwrite(&kfb->maxiter, sizeof(int), 1, out);
  fwrite(kfb->trans, kfb->width * kfb->height * sizeof(float), 1, out);
  return kfb->hasde ? 1 == fwrite(kfb->de, kfb->width * kfb->height * sizeof(float), 1, out) : 1;
}

extern size_t kfb_width(const kfb *kfb) {
  assert(kfb);
  return kfb->width;
}

extern size_t kfb_height(const kfb *kfb) {
  assert(kfb);
  return kfb->height;
}

extern double kfb_maxiter(const kfb *kfb) {
  assert(kfb);
  return kfb->maxiter;
}

extern int kfb_get_hasde(const kfb *kfb) {
  assert(kfb);
  return kfb->hasde;
}

extern double kfb_get(const kfb *kfb, size_t i, size_t j) {
  assert(kfb);
  assert(i < kfb->width);
  assert(j < kfb->height);
  size_t k = kfb->height * i + j;
  int c = kfb->counts[k];
  if (c == kfb->maxiter) {
    return kfb->maxiter;
  } else {
    double t = kfb->trans[k];
    if (t > 1.0) {
      return -c; // glitch
    } else {
      return c + (1.0 - t);
    }
  }
}

extern double kfb_get_de(const kfb *kfb, size_t i, size_t j) {
  assert(kfb);
  assert(i < kfb->width);
  assert(j < kfb->height);
  size_t k = kfb->height * i + j;
  int c = kfb->counts[k];
  if (c == kfb->maxiter) {
    return -1;
  } else {
    return kfb->de[k];
  }
}

static int min(int a, int b) {
  return a < b ? a : b;
}

static int max(int a, int b) {
  return a > b ? a : b;
}

extern double kfb_get_nearest(const kfb *kfb, double i, double j) {
  assert(kfb);
  int i0 = round(i);
  i0 = min(max(i0, 0), kfb->width - 1);
  int j0 = round(j);
  j0 = min(max(j0, 0), kfb->height - 1);
  return kfb_get(kfb, i0, j0);
}

static double linear(double t, double a, double b) {
  return (1 - t) * a + t * b;
}

extern double kfb_get_linear(const kfb *kfb, double i, double j) {
  assert(kfb);
  int i0 = floor(i);
  double x = i - i0;
  int i1 = i0 + 1;
  i0 = min(max(i0, 0), kfb->width - 1);
  i1 = min(max(i1, 0), kfb->width - 1);
  int j0 = floor(j);
  double y = j - j0;
  int j1 = j0 + 1;
  j0 = min(max(j0, 0), kfb->height - 1);
  j1 = min(max(j1, 0), kfb->height - 1);
  double n00 = kfb_get(kfb, i0, j0);
  double n01 = kfb_get(kfb, i0, j1);
  double n10 = kfb_get(kfb, i1, j0);
  double n11 = kfb_get(kfb, i1, j1);
  double n0 = linear(y, n00, n01);
  double n1 = linear(y, n10, n11);
  double n = linear(x, n0, n1);
  return n;
}

static double cubic(double t, double a, double b, double c, double d) {
  const double m[16] = { 0.0,2.0,0.0,0.0, -1.0,0.0,1.0,0.0, 2.0,-5.0,4.0,-1.0, -1.0,3.0,-3.0,1.0 };
  double f[4] = { 1.0, t, t*t, t*t*t };
  double z[4] = { a, b, c, d };
  double r = 0.0;
  for (int i = 0; i < 4; ++i) {
    double s = 0.0;
    for (int j = 0; j < 4; ++j) {
      s += f[i] * m[i * 4 + j];
    }
    r += s * z[i];
  }
  return 0.5 * r;
}

extern double kfb_get_cubic(const kfb *kfb, double i, double j) {
  assert(kfb);
  int i1 = floor(i);
  double x = i - i1;
  int i0 = i1 - 1;
  int i2 = i1 + 1;
  int i3 = i1 + 2;
  i0 = min(max(i0, 0), kfb->width - 1);
  i1 = min(max(i1, 0), kfb->width - 1);
  i2 = min(max(i2, 0), kfb->width - 1);
  i3 = min(max(i3, 0), kfb->width - 1);
  int j1 = floor(j);
  double y = j - j1;
  int j0 = j1 - 1;
  int j2 = j1 + 1;
  int j3 = j1 + 2;
  j0 = min(max(j0, 0), kfb->height - 1);
  j1 = min(max(j1, 0), kfb->height - 1);
  j2 = min(max(j2, 0), kfb->height - 1);
  j3 = min(max(j3, 0), kfb->height - 1);
  double n00 = kfb_get(kfb, i0, j0);
  double n01 = kfb_get(kfb, i0, j1);
  double n02 = kfb_get(kfb, i0, j2);
  double n03 = kfb_get(kfb, i0, j3);
  double n10 = kfb_get(kfb, i1, j0);
  double n11 = kfb_get(kfb, i1, j1);
  double n12 = kfb_get(kfb, i1, j2);
  double n13 = kfb_get(kfb, i1, j3);
  double n20 = kfb_get(kfb, i2, j0);
  double n21 = kfb_get(kfb, i2, j1);
  double n22 = kfb_get(kfb, i2, j2);
  double n23 = kfb_get(kfb, i2, j3);
  double n30 = kfb_get(kfb, i3, j0);
  double n31 = kfb_get(kfb, i3, j1);
  double n32 = kfb_get(kfb, i3, j2);
  double n33 = kfb_get(kfb, i3, j3);
  double n0 = cubic(y, n00, n01, n02, n03);
  double n1 = cubic(y, n10, n11, n12, n13);
  double n2 = cubic(y, n20, n21, n22, n23);
  double n3 = cubic(y, n30, n31, n32, n33);
  double n = cubic(x, n0, n1, n2, n3);
  return n;
}

extern void kfb_set(kfb *kfb, size_t i, size_t j, double n) {
  assert(kfb);
  assert(i < kfb->width);
  assert(j < kfb->height);
  int c;
  float f;
  if (n >= kfb->maxiter) {
    c = kfb->maxiter;
    f = 1.0f;
  } else {
    c = floor(n);
    f = 1.0 - (n - c);
  }
  size_t k = kfb->height * i + j;
  kfb->counts[k] = c;
  kfb->trans[k] = f;
}

extern void kfb_set_hasde(kfb *kfb, int hasde) {
  assert(kfb);
  kfb->hasde = hasde;
}

extern void kfb_set_de(kfb *kfb, size_t i, size_t j, double de) {
  assert(kfb);
  assert(i < kfb->width);
  assert(j < kfb->height);
  int k = kfb->height * i + j;
  kfb->de[k] = de;
}


extern int kfb_get_raw_count(const kfb *kfb, size_t i, size_t j) {
  assert(kfb);
  assert(i < kfb->width);
  assert(j < kfb->height);
  size_t k = kfb->height * i + j;
  int c = kfb->counts[k];
  return c;
}

extern float kfb_get_raw_trans(const kfb *kfb, size_t i, size_t j) {
  assert(kfb);
  assert(i < kfb->width);
  assert(j < kfb->height);
  size_t k = kfb->height * i + j;
  float t = kfb->trans[k];
  return t;
}

extern float kfb_get_raw_de(const kfb *kfb, size_t i, size_t j) {
  assert(kfb);
  assert(i < kfb->width);
  assert(j < kfb->height);
  size_t k = kfb->height * i + j;
  float d = kfb->de[k];
  return d;
}
