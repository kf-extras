#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "pgm.h"

struct pgm {
  int width;
  int height;
  unsigned char *pixels;
};

extern pgm *pgm_alloc(int width, int height) {
  if (! (width > 0 && height > 0)) { return 0; }
  pgm *p = (pgm *) calloc(1, sizeof(struct pgm));
  if (! p) { return 0; }
  p->width = width;
  p->height = height;
  p->pixels = (unsigned char *) malloc(width * height);
  if (! p->pixels) {
    free(p);
    return 0;
  }
  return p;
}

extern void pgm_free(pgm *pgm) {
  if (pgm) {
    if (pgm->pixels) { free(pgm->pixels); }
    free(pgm);
  }
}

extern int pgm_width(const pgm *pgm) {
  assert(pgm);
  return pgm->width;
}

extern int pgm_height(const pgm *pgm) {
  assert(pgm);
  return pgm->height;
}

extern void pgm_set(pgm *pgm, int i, int j, int g) {
  assert(pgm);
  assert(pgm->pixels);
  assert(0 <= i && i < pgm->width);
  assert(0 <= j && j < pgm->height);
  int k = j * pgm->width + i;
  pgm->pixels[k] = g;
}

extern void pgm_setf(pgm *pgm, int i, int j, float g) {
  pgm_set(pgm, i, j, fminf(fmaxf(255 * g, 0), 255));
}

extern int pgm_save(const pgm *pgm, FILE *out) {
  assert(pgm);
  assert(pgm->pixels);
  fprintf(out, "P5\n%d %d\n255\n", pgm->width, pgm->height);
  return 1 == fwrite(pgm->pixels, pgm->width * pgm->height, 1, out);
}
