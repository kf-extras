#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "kfb.h"

const double pi = 3.141592653589793;

void hsv2rgb(float h, float s, float v, float *red, float *grn, float *blu) {
  float i, f, p, q, t, r, g, b;
  if (s == 0) { r = g = b = v; } else {
    h = 6 * (h - floorf(h));
    int ii = i = floorf(h);
    f = h - i;
    p = v * (1 - s);
    q = v * (1 - (s * f));
    t = v * (1 - (s * (1 - f)));
    switch(ii) {
      case 0: r = v; g = t; b = p; break;
      case 1: r = q; g = v; b = p; break;
      case 2: r = p; g = v; b = t; break;
      case 3: r = p; g = q; b = v; break;
      case 4: r = t; g = p; b = v; break;
      default:r = v; g = p; b = q; break;
    }
  }
  *red = r;
  *grn = g;
  *blu = b;
}

void rainbow(const kfb *kfb, size_t i, size_t j, unsigned char *pixel) {
  size_t i0 = i;
  size_t j0 = j;
  size_t ix = i + 1;
  size_t jx = j;
  int sx = 1;
  size_t iy = i;
  size_t jy = j + 1;
  int sy = 1;
  if (ix == kfb_width(kfb)) {
    ix -= 2;
    sx = -1;
  }
  if (jy == kfb_height(kfb)) {
    jy -= 2;
    sx = -1;
  }
  double n0 = kfb_get(kfb, i0, j0);
  float r, g, b;
  if (n0 == kfb_maxiter(kfb)) {
    r = g = b = 1.0f;
  } else {
    double nx = kfb_get(kfb, ix, jx);
    double ny = kfb_get(kfb, iy, jy);
    double zx = sx * (nx - n0);
    double zy = sy * (ny - n0);
    double zz = 1.0;
    double z = sqrt(zx * zx + zy * zy + zz * zz);
    zx /= z;
    zy /= z;
    zz /= z;
    double hue = atan2(zy, zx) / (2 * pi);
    double sat = 0.5 * zz;
    double val = zz;
    hsv2rgb(hue, sat, val, &r, &g, &b);
  }
  size_t k = (kfb_width(kfb) * j + i) * 3;
  pixel[k+0] = fminf(fmaxf(255 * r, 0), 255);
  pixel[k+1] = fminf(fmaxf(255 * g, 0), 255);
  pixel[k+2] = fminf(fmaxf(255 * b, 0), 255);
}

int main(int argc, char **argv) {
  (void) argc;
  (void) argv;
  kfb *kfb = kfb_load(stdin);
  if (! kfb) { return 1; }
  size_t width = kfb_width(kfb);
  size_t height = kfb_height(kfb);
  unsigned char *pixels = (unsigned char *) malloc(width * height * 3);
  #pragma omp parallel for
  for (size_t i = 0; i < width; ++i) {
    for (size_t j = 0; j < height; ++j) {
      rainbow(kfb, i, j, pixels);
    }
  }
  fprintf(stdout, "P6\n%ld %ld\n255\n", width, height);
  fwrite(pixels, width * height * 3, 1, stdout);
  free(pixels);
  kfb_free(kfb);
  return 0;
}
