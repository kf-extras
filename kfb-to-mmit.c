#include <stdio.h>
#include <stdlib.h>

#include "kfb.h"

int main(int argc, char **argv) {
  (void) argc;
  (void) argv;
  kfb *kfb = kfb_load(stdin);
  if (! kfb) { return 1; }
  int width = kfb_width(kfb);
  int height = kfb_height(kfb);
  double *mmit = (double *) malloc(width * height * sizeof(double));
  if (! mmit) { kfb_free(kfb); return 1; }
  #pragma omp parallel for
  for (int j = 0; j < height; ++j) {
    for (int i = 0; i < width; ++i) {
      int k = j * width + i;
      mmit[k] = kfb_get(kfb, i, j);
    }
  }
  // FIXME check sizeof(int) == 4, little-endian
  // FIXME check sizeof(double) == 8, little-endian, IEEE
  // FIXME check write success
  fwrite(&width, sizeof(width), 1, stdout);
  fwrite(&height, sizeof(height), 1, stdout);
  fwrite(mmit, width * height * sizeof(double), 1, stdout);
  free(mmit);
  kfb_free(kfb);
  return 0;
}
