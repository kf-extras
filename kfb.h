#ifndef KFB_H
#define KFB_H 1

#include <stdio.h>

struct kfb;
typedef struct kfb kfb;

extern kfb *kfb_load(FILE *in);
extern kfb *kfb_alloc(const kfb *proto, size_t width, size_t height);
extern void kfb_free(kfb *k);
extern int kfb_save(const kfb *kfb, FILE *out);
extern size_t kfb_width(const kfb *kfb);
extern size_t kfb_height(const kfb *kfb);
extern double kfb_maxiter(const kfb *kfb);
extern double kfb_get(const kfb *kfb, size_t i, size_t j);
extern double kfb_get_nearest(const kfb *kfb, double i, double j);
extern double kfb_get_linear(const kfb *kfb, double i, double j);
extern double kfb_get_cubic(const kfb *kfb, double i, double j);
extern void kfb_set(kfb *kfb, size_t i, size_t j, double n);

extern int kfb_get_hasde(const kfb *kfb);
extern void kfb_set_hasde(kfb *kfb, int hasde);
extern double kfb_get_de(const kfb *kfb, size_t i, size_t j);
extern void kfb_set_de(kfb *kfb, size_t i, size_t j, double de);

extern int   kfb_get_raw_count(const kfb *kfb, size_t i, size_t j);
extern float kfb_get_raw_trans(const kfb *kfb, size_t i, size_t j);
extern float kfb_get_raw_de   (const kfb *kfb, size_t i, size_t j);

#endif
