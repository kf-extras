#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "kfb.h"
#include "pgm.h"

int cmp_double(const void *a, const void *b) {
  const double *x = (const double *) a;
  const double *y = (const double *) b;
  double p = *x;
  double q = *y;
  if (p < q) return -1;
  if (p > q) return  1;
  return 0;
}

void histogram(const kfb *kfb, int i, int j, const double *histo, int nhisto, double density, pgm *pgm) {
  float g = 0.0f;
  double n0 = kfb_get(kfb, i, j);
  if (n0 != kfb_maxiter(kfb)) {
    double *np = (double *) bsearch(&n0, histo, nhisto, sizeof(double), cmp_double);
    g = 1.0 - pow(1.0 - (np - histo) / (double) nhisto, 1.0 / density);
  }
  pgm_setf(pgm, i, j, g);
}

int main(int argc, char **argv) {
  double density = 1.0;
  if (argc > 1) {
    density = fmax(1.0, atof(argv[1]));
  }
  kfb *kfb = kfb_load(stdin);
  if (! kfb) { return 1; }
  int width = kfb_width(kfb);
  int height = kfb_height(kfb);
  int maxiter = kfb_maxiter(kfb);
  double *histo = (double *) malloc(width * height * sizeof(double));
  int k = 0;
  #pragma omp parallel for
  for (int i = 0; i < width; ++i) {
    for (int j = 0; j < height; ++j) {
      int kk;
      #pragma omp atomic capture
      kk = k++;
      histo[kk] = kfb_get(kfb, i, j);
    }
  }
  qsort(histo, width * height, sizeof(double), cmp_double);
  int nhisto;
  for (nhisto = width * height; nhisto > 0; --nhisto) {
    if (histo[nhisto-1] < maxiter) {
      break;
    }
  }
  pgm *pgm = pgm_alloc(width, height);
  #pragma omp parallel for
  for (int i = 0; i < width; ++i) {
    for (int j = 0; j < height; ++j) {
      histogram(kfb, i, j, histo, nhisto, density, pgm);
    }
  }
  pgm_save(pgm, stdout);
  pgm_free(pgm);
  free(histo);
  kfb_free(kfb);
  return 0;
}
