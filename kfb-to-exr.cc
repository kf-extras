#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <ImfNamespace.h>
#include <ImfOutputFile.h>
#include <ImfIntAttribute.h>
#include <ImfChannelList.h>

#include "kfb.h"

namespace IMF = OPENEXR_IMF_NAMESPACE;
using namespace IMF;

bool writeEXR(const char *filename, int width, int height, int maxiter, const uint32_t *n, const float *nf, const float *de)
{
  if (n == nullptr && nf == nullptr && de == nullptr) return false;
  Header header(width, height);
  header.insert("Iterations", IntAttribute(maxiter));
  if (n  != nullptr) header.channels().insert("N",  Channel(IMF::UINT));
  if (nf != nullptr) header.channels().insert("NF", Channel(IMF::FLOAT));
  if (de != nullptr) header.channels().insert("DE", Channel(IMF::FLOAT));
  OutputFile of(filename, header);
  FrameBuffer fb;
  if (n  != nullptr) fb.insert("N",  Slice(IMF::UINT,  (char *) n,  sizeof(*n ) * 1, sizeof(*n ) * width));
  if (nf != nullptr) fb.insert("NF", Slice(IMF::FLOAT, (char *) nf, sizeof(*nf) * 1, sizeof(*nf) * width));
  if (de != nullptr) fb.insert("DE", Slice(IMF::FLOAT, (char *) de, sizeof(*de) * 1, sizeof(*de) * width));
  of.setFrameBuffer(fb);
  of.writePixels(height);
  return true;
}

int main(int argc, char **argv) {
  if (argc < 3) return 1;
  std::FILE *in = std::fopen(argv[1], "rb");
  if (! in) return 1;
  kfb *kfb = kfb_load(in);
  if (! kfb) { return 1; }
  int width = kfb_width(kfb);
  int height = kfb_height(kfb);
  int maxiter = kfb_maxiter(kfb);
  uint32_t *n  = (uint32_t *) std::malloc(sizeof(*n ) * width * height);
  float    *nf = (float    *) std::malloc(sizeof(*nf) * width * height);
  float    *de = kfb_get_hasde(kfb)
               ? (float    *) std::malloc(sizeof(*de) * width * height)
               : nullptr;
  size_t glitched = 0;
  #pragma omp parallel for reduction(+:glitched)
  for (int j = 0; j < height; ++j) {
    for (int i = 0; i < width; ++i) {
      size_t k = (size_t) j * width + i;
      int   count = kfb_get_raw_count(kfb, i, j);
      float trans = kfb_get_raw_trans(kfb, i, j);
      float dist  = de ? kfb_get_raw_de(kfb, i, j) : 0.0f;
      if (count == maxiter)
      {
        // unescaped
        n[k] = 0xFFffFFffU;
        nf[k] = 0.0f;
        if (de) de[k] = 0.0f;
      }
      else if (trans > 1.0f)
      {
        // glitch
        glitched += 1;
        n[k] = count;
        nf[k] = 0.0f;
        if (de) de[k] = 0.0f;
      }
      else
      {
        // escaped, ok
        n[k] = count;
        nf[k] = 1.0f - trans;
        if (de) de[k] = dist;
      }
    }
  }
  if (glitched)
  {
    std::cerr << argv[0] << ": WARNING " << glitched << "/" << ((size_t) width * height) << " pixels are glitched!" << std::endl;
  }
  bool ok = writeEXR(argv[2], width, height, maxiter, n, nf, de);
  return ! ok;
}
