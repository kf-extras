#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "kfb.h"
#include "pgm.h"

typedef double float1;

struct float2
{
  double x, y;
};

float2 float2_(double x, double y)
{
  float2 m;
  m.x = x;
  m.y = y;
  return m;
}

float2 operator+(float2 u, float2 v)
{
  float2 w;
  w.x = u.x + v.x;
  w.y = u.y + v.y;
  return w;
}

float2 operator/(float2 u, double v)
{
  float2 w;
  w.x = u.x / v;
  w.y = u.y / v;
  return w;
}

double dot(float2 u, float2 v)
{
  return u.x * v.x + u.y * v.y;
}

struct float3
{
  double x, y, z;
};

float3 float3_(float1 x, float1 y, float1 z)
{
  float3 m;
  m.x = x;
  m.y = y;
  m.z = z;
  return m;
}

float3 operator/(float3 u, double v)
{
  float3 w;
  w.x = u.x / v;
  w.y = u.y / v;
  w.z = u.z / v;
  return w;
}

float3 operator-(float3 v)
{
  float3 w;
  w.x = - v.x;
  w.y = - v.y;
  w.z = - v.z;
  return w;
}

float3 operator-(float3 u, float3 v)
{
  float3 w;
  w.x = u.x - v.x;
  w.y = u.y - v.y;
  w.z = u.z - v.z;
  return w;
}

float3 operator+(float3 u, float3 v)
{
  float3 w;
  w.x = u.x + v.x;
  w.y = u.y + v.y;
  w.z = u.z + v.z;
  return w;
}

double dot(float3 u, float3 v)
{
  return u.x * v.x + u.y * v.y + u.z * v.z;
}

double length(float3 v)
{
  return sqrt(dot(v, v));
}

float3 normalize(float3 v)
{
  return v / length(v);
}

float3 cross(float3 u, float3 v)
{
  float3 w;
  w.x = u.y * v.z - u.z * v.y;
  w.y = u.z * v.x - u.x * v.z;
  w.z = u.x * v.y - u.y * v.x;
  return w;
}

struct float3x3
{
  float3 x, y, z; // columns
};

float3x3 float3x3_(float3 x, float3 y, float3 z)
{
  float3x3 m;
  m.x = x;
  m.y = y;
  m.z = z;
  return m;
}

float3 mul(float3x3 m, float3 v)
{
  float3 w;
  w.x = m.x.x * v.x + m.y.x * v.y + m.z.x * v.z;
  w.y = m.x.y * v.x + m.y.y * v.y + m.z.y * v.z;
  w.z = m.x.z * v.x + m.y.z * v.y + m.z.z * v.z;
  return w;
}

struct float4
{
  double x, y, z, w;
};

float4 float4_(double x, double y, double z, double w)
{
  float4 o;
  o.x = x;
  o.y = y;
  o.z = z;
  o.w = w;
  return o;
}

double dot(float4 u, float4 v)
{
  return u.x * v.x + u.y * v.y + u.z * v.z + u.w * v.w;
}

float4 operator*(double u, float4 v)
{
  float4 o;
  o.x = u * v.x;
  o.y = u * v.y;
  o.z = u * v.z;
  o.w = u * v.w;
  return o;
}

typedef double float4x4[4][4];

float4 mul(float4 v, float4x4 m)
{
  float4 w;
  w.x = v.x * m[0][0] + v.y * m[0][1] + v.z * m[0][2] + v.w * m[0][3];
  w.y = v.x * m[1][0] + v.y * m[1][1] + v.z * m[1][2] + v.w * m[1][3];
  w.z = v.x * m[2][0] + v.y * m[2][1] + v.z * m[2][2] + v.w * m[2][3];
  w.w = v.x * m[3][0] + v.y * m[3][1] + v.z * m[3][2] + v.w * m[3][3];
  return w;
}

#define TRIANGLE_VERTICES 3
#define LOCAL_X 0
#define LOCAL_Y 1
#define LOCAL_Z 2

// http://graphics.zcu.cz/sscurvature.html
			float2 CalcCurvature_ShapeOperator_2edge(float3 a, float3 b, float3 c, float3 an, float3 bn, float3 cn)
			{
	
				float3 local_trianglePos[TRIANGLE_VERTICES];
				float3 local_triangleNor[TRIANGLE_VERTICES];

	
				//=========================================================================================================

				float3 _ba_ = (b - a);
				float3 _ca_ = (c - a);

	
	
				float3 axis[TRIANGLE_VERTICES];
				axis[LOCAL_X] = normalize(_ba_);
				axis[LOCAL_Z] = normalize(cross(axis[LOCAL_X], _ca_));    
				axis[LOCAL_Y] = normalize(cross(axis[LOCAL_X], axis[LOCAL_Z]));




				float3x3 transformMatrix = (float3x3_(axis[LOCAL_X], axis[LOCAL_Y], axis[LOCAL_Z]));  //set columns
				

				local_trianglePos[0] = float3_(0, 0, 0);
				local_triangleNor[0] = mul(transformMatrix, an);
	
				local_trianglePos[1] = mul(transformMatrix, _ba_);	
				local_triangleNor[1] = mul(transformMatrix, bn);

				local_trianglePos[2] = mul(transformMatrix, _ca_);	
				local_triangleNor[2] = mul(transformMatrix, cn);
	
				float2 u;
				float2 v;

				float2 du;
				float2 dv;

				u.x = local_trianglePos[1].x - local_trianglePos[0].x;
				u.y = local_trianglePos[2].x - local_trianglePos[1].x;
	
				v.x = local_trianglePos[1].y - local_trianglePos[0].y;
				v.y = local_trianglePos[2].y - local_trianglePos[1].y;
		
				du.x = local_triangleNor[1].x - local_triangleNor[0].x;
				du.y = local_triangleNor[2].x - local_triangleNor[1].x;
	
				dv.x = local_triangleNor[1].y - local_triangleNor[0].y;
				dv.y = local_triangleNor[2].y - local_triangleNor[1].y;	
		
				//===============================================================================
				

				//inverse of 3x3 matrix
				//invB = inverse of B

				float1 aa = dot(u, u);
				float1 bb = dot(u, v);
				float1 cc = dot(v, v);

				float1 invB_B = -(bb * cc);
				float1 invB_C =  (bb * bb);
				float1 invB_D =  (aa * cc);
				float1 invB_E = -(aa * bb);
				float1 invB_A = -invB_C + (cc * cc) + invB_D;
				float1 invB_F =  (aa * aa) + invB_D - invB_C;

				float1 determinantB = 1.0 / ((aa + cc) * (invB_D - invB_C));



				//=======================================================================================

				float4x4 tmp;
	
	
				tmp[0][0] = v.x * invB_B + u.x * invB_A;
				tmp[1][0] = v.x * invB_C + u.x * invB_B;
				tmp[2][0] = v.y * invB_B + u.y * invB_A;
				tmp[3][0] = v.y * invB_C + u.y * invB_B;


				tmp[0][1] = v.x * invB_D + u.x * invB_B;
				tmp[1][1] = v.x * invB_E + u.x * invB_D;
				tmp[2][1] = v.y * invB_D + u.y * invB_B;
				tmp[3][1] = v.y * invB_E + u.y * invB_D;


				tmp[0][2] = v.x * invB_E + u.x * invB_C;
				tmp[1][2] = v.x * invB_F + u.x * invB_E;
				tmp[2][2] = v.y * invB_E + u.y * invB_C;
				tmp[3][2] = v.y * invB_F + u.y * invB_E;


				tmp[0][3] = 0;
				tmp[1][3] = 0;
				tmp[2][3] = 0;
				tmp[3][3] = 0;


				//================================================================================

				float4 tmp_gpu = determinantB * mul(float4_(du.x, dv.x, du.y, dv.y), tmp);
				
				//================================================================================


				float1 D = dot(tmp_gpu, tmp_gpu) + (3.0 * tmp_gpu.y * tmp_gpu.y - 2.0 * tmp_gpu.x * tmp_gpu.z);
	
				if (D < 0) D = 0;
				float1 sqrtD = sqrt(D);

				float1 L1 = ((tmp_gpu.x + tmp_gpu.z) + sqrtD) * 0.5;
				float1 L2 = ((tmp_gpu.x + tmp_gpu.z) - sqrtD) * 0.5;


				float1 gauss = L1 * L2;
	
				float1 mean = 0.5 * (L1 + L2);

				return float2_(gauss, mean);
			}




void curvature(const kfb *kfb, int i, int j, int xi, int xj, int yi, int yj, float3 *normals, float2 *curves) {
  int i0 = i;
  int j0 = j;
  int ix = i + xi;
  int jx = j + xj;
  int iy = i + yi;
  int jy = j + yj;
  int w = kfb_width(kfb);
  int h = kfb_height(kfb);
  if (ix == w) {
    ix -= 2;
  }
  if (ix == -1) {
    ix += 2;
  }
  if (iy == w) {
    iy -= 2;
  }
  if (iy == -1) {
    iy += 2;
  }
  if (jx == h) {
    jx -= 2;
  }
  if (jx == -1) {
    jx += 2;
  }
  if (jy == h) {
    jy -= 2;
  }
  if (jy == -1) {
    jy += 2;
  }
  double n0 = kfb_get(kfb, i0, j0);
  double nx = kfb_get(kfb, ix, jx);
  double ny = kfb_get(kfb, iy, jy);
  if (n0 != kfb_maxiter(kfb)) {
    float3 a = float3_(i0, j0, log(n0));
    float3 b = float3_(ix, jx, log(nx));
    float3 c = float3_(iy, jy, log(ny));
    float3 an = normals[i0 * h + j0];
    float3 bn = normals[ix * h + jx];
    float3 cn = normals[iy * h + jy];
    *curves = *curves + CalcCurvature_ShapeOperator_2edge(a, b, c, an, bn, cn);
  }
}


void normal(const kfb *kfb, int i, int j, int xi, int xj, int yi, int yj, float3 *normals) {
  int i0 = i;
  int j0 = j;
  int ix = i + xi;
  int jx = j + xj;
  int iy = i + yi;
  int jy = j + yj;
  int w = kfb_width(kfb);
  int h = kfb_height(kfb);
  if (ix == w) {
    ix -= 2;
  }
  if (ix == -1) {
    ix += 2;
  }
  if (iy == w) {
    iy -= 2;
  }
  if (iy == -1) {
    iy += 2;
  }
  if (jx == h) {
    jx -= 2;
  }
  if (jx == -1) {
    jx += 2;
  }
  if (jy == h) {
    jy -= 2;
  }
  if (jy == -1) {
    jy += 2;
  }
  double n0 = kfb_get(kfb, i0, j0);
  double nx = kfb_get(kfb, ix, jx);
  double ny = kfb_get(kfb, iy, jy);
  if (n0 != kfb_maxiter(kfb)) {
    float3 a = float3_(i0, j0, log(n0));
    float3 b = float3_(ix, jx, log(nx));
    float3 c = float3_(iy, jy, log(ny));
    float3 u = normalize(b - a);
    float3 v = normalize(c - a);
    float3 w = normalize(cross(u, v));
    if (w.z < 0)
    {
      w = -w;
    }
    *normals = *normals + w;
  }
} 

int main(int argc, char **argv) {
  (void) argc;
  (void) argv;
  kfb *kfb = kfb_load(stdin);
  if (! kfb) { return 1; }
  int width = kfb_width(kfb);
  int height = kfb_height(kfb);
  pgm *pgm = pgm_alloc(width, height);
  if (! pgm) { kfb_free(kfb); return 1; }
  float3 *normals = (float3 *) calloc(1, width * height * sizeof(*normals));
  float2 *curves = (float2 *) calloc(1, width * height * sizeof(*curves));
  #pragma omp parallel for
  for (int i = 0; i < width; ++i) {
    for (int j = 0; j < height; ++j) {
      int k = i * height + j;
      normal(kfb, i, j, 1, 0, 0, 1, normals+k);
//      normal(kfb, i, j, 1, 0, 1, 1, normals+k);
//      normal(kfb, i, j, 1, 1, 0, 1, normals+k);
      normal(kfb, i, j, 1, 0, 0, -1, normals+k);
//      normal(kfb, i, j, 1, 0, 1, -1, normals+k);
//      normal(kfb, i, j, 1, -1, 0, -1, normals+k);
      normal(kfb, i, j, -1, 0, 0, 1, normals+k);
//      normal(kfb, i, j, -1, 0, -1, 1, normals+k);
//      normal(kfb, i, j, -1, 1, 0, 1, normals+k);
      normal(kfb, i, j, -1, 0, 0, -1, normals+k);
//      normal(kfb, i, j, -1, 0, -1, -1, normals+k);
//      normal(kfb, i, j, -1, -1, 0, -1, normals+k);
    }
  }
  #pragma omp parallel for
  for (int k = 0; k < width * height; ++k) {
    normals[k] = normalize(normals[k]);
  }
  #pragma omp parallel for
  for (int i = 0; i < width; ++i) {
    for (int j = 0; j < height; ++j) {
      int k = i * height + j;
      curvature(kfb, i, j, 1, 0, 0, 1, normals, curves+k);
      curvature(kfb, i, j, 1, 0, 0, -1, normals, curves+k);
      curvature(kfb, i, j, -1, 0, 0, 1, normals, curves+k);
      curvature(kfb, i, j, -1, 0, 0, -1, normals, curves+k);
    }
  }
  #pragma omp parallel for
  for (int k = 0; k < width * height; ++k) {
    curves[k] = curves[k] / 4.0;
  }
  #pragma omp parallel for
  for (int i = 0; i < width; ++i) {
    for (int j = 0; j < height; ++j) {
      double c = curves[i * height + j].x * 100;
      if (c > 0) { c =  log(1 + c); }
      else       { c = -log(1 - c); }
      pgm_setf(pgm, i, j, 0.5 + 0.5 * c);
    }
  }
  pgm_save(pgm, stdout);
  #pragma omp parallel for
  for (int i = 0; i < width; ++i) {
    for (int j = 0; j < height; ++j) {
      double c = curves[i * height + j].y * 100;
      if (c > 0) { c =  log(1 + c); }
      else       { c = -log(1 - c); }
      pgm_setf(pgm, i, j, 0.5 + 0.5 * c);
    }
  }
  pgm_save(pgm, stdout);
  free(curves);
  free(normals);
  pgm_free(pgm);
  kfb_free(kfb);
  return 0;
}
