#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "kfb.h"

int cmp_double(const void *a, const void *b) {
  const double *x = (const double *) a;
  const double *y = (const double *) b;
  double p = *x;
  double q = *y;
  if (p < q) return -1;
  if (p > q) return  1;
  return 0;
}

int main(int argc, char **argv) {
  (void) argc;
  (void) argv;
  kfb *kfb = kfb_load(stdin);
  if (! kfb) { return 1; }
  int width = kfb_width(kfb);
  int height = kfb_height(kfb);
  int maxiter = kfb_maxiter(kfb);
  double *histo = (double *) malloc(width * height * sizeof(double));
  int k = 0;
  #pragma omp parallel for
  for (int i = 0; i < width; ++i) {
    for (int j = 0; j < height; ++j) {
      int kk;
      #pragma omp atomic capture
      kk = k++;
      histo[kk] = kfb_get(kfb, i, j);
    }
  }
  qsort(histo, width * height, sizeof(double), cmp_double);
  int nhisto;
  for (nhisto = width * height; nhisto > 0; --nhisto) {
    if (histo[nhisto-1] < maxiter) {
      break;
    }
  }
  int mhisto;
  for (mhisto = 0; mhisto < nhisto; ++mhisto)
  {
    if (histo[mhisto] >= 0) {
      break;
    }
  }
  printf("%3.6f%%\t%d glitched pixels\n", mhisto * 100.0 / (width * height), mhisto);
  printf("%3.6f%%\t%d unescaped pixels\n", (width * height - nhisto) * 100.0 / (width * height), width * height - nhisto);
  printf("percentile\titerations\n");
  for (k = 0; k <= 10; ++k) {
    int l = round(mhisto + k / 10.0 * (nhisto - 1 - mhisto));
    assert(0 <= l);
    assert(l < width * height);
    printf("%12d%%\t%.3f\n", 10 * k, histo[l]);
  }
  free(histo);
  kfb_free(kfb);
  return 0;
}
