#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "kfb.h"
#include "pgm.h"

void pseudode(const kfb *kfb, size_t i, size_t j, pgm *pgm) {
  size_t i0 = i;
  size_t j0 = j;
  size_t ix = i + 1;
  size_t jx = j;
  int sx = 1;
  size_t iy = i;
  size_t jy = j + 1;
  int sy = 1;
  if (ix == kfb_width(kfb)) {
    ix -= 2;
    sx = -1;
  }
  if (jy == kfb_height(kfb)) {
    jy -= 2;
    sx = -1;
  }
  double n0 = kfb_get(kfb, i0, j0);
  float g;
  if (n0 == kfb_maxiter(kfb)) {
    g = 1.0f;
  } else {
    double nx = kfb_get(kfb, ix, jx);
    double ny = kfb_get(kfb, iy, jy);
    double zx = sx * (nx - n0);
    double zy = sy * (ny - n0);
    double zz = 1.0;
    double z = sqrt(zx * zx + zy * zy + zz * zz);
    g = zz / z;
  }
  pgm_setf(pgm, i, j, g);
}

int main(int argc, char **argv) {
  (void) argc;
  (void) argv;
  kfb *kfb = kfb_load(stdin);
  if (! kfb) { return 1; }
  size_t width = kfb_width(kfb);
  size_t height = kfb_height(kfb);
  pgm *pgm = pgm_alloc(width, height);
  if (! pgm) { kfb_free(kfb); return 1; }
  #pragma omp parallel for
  for (size_t i = 0; i < width; ++i) {
    for (size_t j = 0; j < height; ++j) {
      pseudode(kfb, i, j, pgm);
    }
  }
  pgm_save(pgm, stdout);
  pgm_free(pgm);
  kfb_free(kfb);
  return 0;
}
