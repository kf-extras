EXES = \
	kfb-curvature \
	kfb-de-histogram \
	kfb-expmap \
	kfb-histogram \
	kfb-pseudo-de \
	kfb-rainbow \
	kfb-resize \
	kfb-statistics \
	kfb-stretch \
	kfb-to-mmit \
	kfb-to-exr

SCRIPTS = \
	kfb-to-ogv.sh

all: $(EXES)

clean:
	-rm $(EXES)

prefix ?= $(HOME)

install: $(EXES) $(SCRIPTS)
	cp -avf $(EXES) $(SCRIPTS) $(prefix)/bin/

.SUFFIXES:
.PHONY: all clean install

%: %.o kfb.o pgm.o
	g++ -std=c++17 -Wall -Wextra -pedantic -march=native -O3 -fopenmp -o $@ $< kfb.o pgm.o -lm -ggdb

%.o: %.c kfb.h pgm.h
	g++ -std=c++17 -Wall -Wextra -pedantic -march=native -O3 -fopenmp -o $@ -c $< -lm -ggdb

%.o: %.cc kfb.h pgm.h
	g++ -std=c++17 -Wall -Wextra -pedantic -march=native -O3 -fopenmp -o $@ -c $< -lm -ggdb

kfb-to-exr: kfb-to-exr.cc kfb.h kfb.o
	g++ -std=c++11 -Wall -Wextra -pedantic -Wno-deprecated -Wno-unused-parameter -march=native -O3 -fopenmp -o $@ $< kfb.o `pkg-config --cflags --libs OpenEXR` -lm -ggdb
