#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "kfb.h"
#include "pgm.h"

void stretch(const kfb *kfb, int i, int j, double nmin, double nmax, pgm *pgm) {
  float g = 0.0f;
  double n = kfb_get(kfb, i, j);
  if (n != kfb_maxiter(kfb)) {
    g = (n - nmin) / (nmax - nmin);
  }
  pgm_setf(pgm, i, j, g);
}

int main(int argc, char **argv) {
  (void) argc;
  (void) argv;
  kfb *kfb = kfb_load(stdin);
  if (! kfb) { return 1; }
  int width = kfb_width(kfb);
  int height = kfb_height(kfb);
  double maxiter = kfb_maxiter(kfb);
  double nmax = -1.0 / 0.0;
  double nmin =  1.0 / 0.0;
  for (int i = 0; i < width; ++i) {
    for (int j = 0; j < height; ++j) {
      double n = kfb_get(kfb, i, j);
      if (n != maxiter) {
        nmax = fmax(n, nmax);
        nmin = fmin(n, nmin);
      }
    }
  }
  pgm *pgm = pgm_alloc(width, height);
  if (! pgm) { kfb_free(kfb); return 1; }
  #pragma omp parallel for
  for (int i = 0; i < width; ++i) {
    for (int j = 0; j < height; ++j) {
      stretch(kfb, i, j, nmin, nmax, pgm);
    }
  }
  pgm_save(pgm, stdout);
  pgm_free(pgm);
  kfb_free(kfb);
  return 0;
}
