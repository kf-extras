#define _POSIX_C_SOURCE 200809L
// glibc feature-test macro for getline

#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "kfb.h"

const double pi = 3.141592653589793;

struct expmap {
  FILE *in;
  char *lineptr;
  size_t linesize;
  size_t width;
  size_t height;
  int circumference;
  int radius;
  double maxiter;
  int row;
  kfb *kfb_;
};
typedef struct expmap expmap;

int expmap_load(expmap *expmap, int pass) {
  if (! expmap) {
    return 0;
  }
  if (expmap->kfb_) {
    kfb_free(expmap->kfb_);
    expmap->kfb_ = 0;
  }
  int len = getline(&expmap->lineptr, &expmap->linesize, expmap->in);
  if (len == -1) {
    expmap->lineptr = 0;
    return 0;
  }
  if (len > 0) {
    if (expmap->lineptr[len - 1] == '\n') {
      expmap->lineptr[len - 1] = 0;
    }
    FILE *kin = fopen(expmap->lineptr, "rb");
    free(expmap->lineptr);
    expmap->lineptr = 0;
    if (! kin) {
      return 0;
    }
    expmap->kfb_ = kfb_load(kin);
    fclose(kin);
    if (! expmap->kfb_) {
      return 0;
    }
    expmap->maxiter = kfb_maxiter(expmap->kfb_);
    expmap->row = 0;
    if (pass > 0) {
      if (! (expmap->width == kfb_width(expmap->kfb_) && expmap->height == kfb_height(expmap->kfb_))) {
        return 0;
      }
    }
    expmap->width = kfb_width(expmap->kfb_);
    expmap->height = kfb_height(expmap->kfb_);
    expmap->circumference = expmap->height * pi / 2;
    // 0.5 ^ (1 - 1 / radius) - 0.5 = |(0.5, 0)-(0.5*cos(t), 0.5*sin(t))| where t = 2 * pi / circumference
    double t = 2 * pi / expmap->circumference;
    double dx = 0.5 - 0.5 * cos(t);
    double dy = 0.5 * sin(t);
    double d = sqrt(dx * dx + dy * dy);
    // (1 - 1 / radius) log 0.5 = log (d + 0.5)
    // 1 - 1 / radius = log (d + 0.5) / log 0.5
    // 1 / radius = 1 - log (d + 0.5) / log 0.5
    expmap->radius = 1.0 / (1.0 - log(d + 0.5) / log(0.5));
    return 1;
  }
  return 0;
}

int expmap_get_circumference(const expmap *expmap) {
  assert(expmap);
  return expmap->circumference;
}

int expmap_get_radius(const expmap *expmap) {
  assert(expmap);
  return expmap->radius;
}

double expmap_get_maxiter(const expmap *expmap) {
  assert(expmap);
  return expmap->maxiter;
}

expmap *expmap_create(FILE *in) {
  if (! in) { return 0; }
  expmap *expmap = (struct expmap *) calloc(1, sizeof(struct expmap));
  if (! expmap) { return 0; }
  memset(expmap, 0, sizeof(struct expmap));
  expmap->in = in;
  if (! expmap_load(expmap, 0)) { free(expmap); return 0; }
  return expmap;
}

void expmap_free(expmap *expmap) {
  if (expmap) {
    if (expmap->lineptr) { free(expmap->lineptr); }
    if (expmap->kfb_) { kfb_free(expmap->kfb_); }
    free(expmap);
  }
}

double *expmap_get_row(expmap *expmap) {
  if (! expmap) {
    return 0;
  }
  if (expmap->row >= expmap->radius) {
    if (! expmap_load(expmap, 1)) {
      return 0;
    }
  }
  double *row = (double *) malloc(expmap->circumference * sizeof(double));
  if (! row) {
    return 0;
  }
  #pragma omp parallel for
  for (int k = 0; k < expmap->circumference; ++k) {
    double r = pow(0.5, 1.0 - expmap->row / (double) expmap->radius);
    double t = 2 * pi * k / (double) expmap->circumference;
    double i = r * cos(t) * expmap->height / 2.0 + expmap->width / 2.0;
    double j = r * sin(t) * expmap->height / 2.0 + expmap->height / 2.0;
    row[k] = kfb_get_nearest(expmap->kfb_, i, j);
  }
  expmap->row++;
  return row;
}

void expmap_free_row(double *row) {
  if (row) { free(row); }
}

void pseudode(const double *row0, const double *row1, double maxiter0, double maxiter1, int circumference, unsigned char *pixels) {
  (void) maxiter0;
  #pragma omp parallel for
  for (int k = 0; k < circumference; ++k) {
    int k1 = (k + 1) % circumference;
    float g;
    double n0 = row1[k];
    if (n0 >= maxiter1) {
      g = 1.0f;
    } else {
      double nx = row1[k1];
      double ny = row0[k];
      double zx = nx - n0;
      double zy = ny - n0;
      double zz = 1.0;
      double z = sqrt(zx * zx + zy * zy + zz * zz);
      g = zz / z;
    }
    pixels[k] = fminf(fmaxf(255.0f * g, 0.0f), 255.0f);
  }
}

int main(int argc, char **argv) {
  int count;
  if (argc > 1) {
    count = atoi(argv[1]);
    if (! (count > 0)) { return 1; }
  } else {
    return 1;
  }
  expmap *expmap = expmap_create(stdin);
  if (! expmap) { return 1; }
  int circumference = expmap_get_circumference(expmap);
  int radius = expmap_get_radius(expmap);
  int width = circumference;
  int height = radius * count - 1; // -1 for pseudo-de colouring
  fprintf(stderr, "width: %d\nheight: %d per kfb\nheight: %d total\n", circumference, radius, height);
  unsigned char *pixels = (unsigned char *) malloc(width);
  if (! pixels) {
    expmap_free(expmap);
    return 1;
  }
  fprintf(stdout, "P5\n%d %d\n255\n", width, height);
  double *rows[2];
  rows[0] = 0;
  rows[1] = expmap_get_row(expmap);
  double maxiter[2];
  maxiter[0] = 0;
  maxiter[1] = expmap_get_maxiter(expmap);
  while (1) {
    if (rows[0]) { expmap_free_row(rows[0]); }
    rows[0] = rows[1];
    rows[1] = expmap_get_row(expmap);
    if (! rows[1]) { break; }
    maxiter[0] = maxiter[1];
    maxiter[1] = expmap_get_maxiter(expmap);
    pseudode(rows[0], rows[1], maxiter[0], maxiter[1], circumference, pixels);
    fwrite(pixels, width, 1, stdout); // FIXME check success
  }
  if (rows[0]) { expmap_free_row(rows[0]); }
  expmap_free(expmap);
  free(pixels);
  return 0;
}
