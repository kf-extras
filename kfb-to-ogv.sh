#!/bin/bash

# change these lines to suit your environment
zoom="${HOME}/code/maximus_book/code/zoom"

# change these lines to suit your taste
sr=8000
aa=2

seconds="${1}"

work="${2}"
if [ "${work}x" == "x" ]
then
  work="$(pwd)"
fi

tmp="${3}"
if [ "${tmp}x" == "x" ]
then
  tmp="${work}"
fi

echo "checking required programs"
(
which kfb-pseudo-de || echo "FAIL kfb-pseudo-de"
which kfb-expmap || echo "FAIL kfb-expmap"
test -x "${zoom}" || echo "FAIL zoom (book)"
which pnmscale || echo "FAIL pnmscale"
which pgmtoppm || echo "FAIL pgmtoppm"
which pnmflip || echo "FAIL pnmflip"
which ecasound || echo "FAIL ecasound"
which oggenc || echo "FAIL oggenc"
which avconv || echo "FAIL avconv"
) | tee "${tmp}/programs.log"
if grep -q "^FAIL" "${tmp}/programs.log"
then
  echo "some programs missing, aborting"
  exit 1
fi

pushd "${work}"

count="$(ls *.kfb | wc -l)"
echo "${count} kfb files"

echo "converting kfb to ppm"
for i in *.kfb
do
  kfb-pseudo-de < "${i}" | pnmscale -reduce "${aa}" 2>/dev/null | pgmtoppm white > "${tmp}/${i%kfb}ppm"
done

echo "generating exponential map"
ls *.kfb | kfb-expmap "${count}" > "${tmp}/expmap.pgm"

head -n 2 "${tmp}/expmap.pgm" | tail -n 1 | ( read ewidth eheight
echo "exponential map is ${ewidth}x${eheight}"

echo "scaling and flipping map"
pnmscale -pixels "$(( seconds * sr ))" < "${tmp}/expmap.pgm" | pnmflip -tb > "${tmp}/expmap2.pgm"

echo "converting map image to audio"
cat "${tmp}/expmap2.pgm" | ecasound -f:u8,1,"${sr}" -i:stdin -f:f32,1,"${sr}" -o:"${tmp}/audio1.wav"
echo "resampling to output sample rate"
ecasound -f:f32,1,48000 -i:resample-hq,auto,"${tmp}/audio1.wav" -o:"${tmp}/audio2.wav"
echo "correcting dc offset"
ecasound -f:f32,1,48000 -i:"${tmp}/audio2.wav" -efh:1 -eaw:33,100 -o:"${tmp}/audio3.wav"
echo "applying reverb (left)"
ecasound -f:f32,1,48000 -i:"${tmp}/audio3.wav" -el:gverb,10.1,5,0.5,0.5,-70,0,-12 -eaw:33,100 -o:"${tmp}/audio3l.wav"
echo "applying reverb (right)"
ecasound -f:f32,1,48000 -i:"${tmp}/audio3.wav" -el:gverb,9.9,5,0.5,0.5,-70,0,-12 -eaw:33,100 -o:"${tmp}/audio3r.wav"
echo "combining audio tracks to stereo"
ecasound -a:1 -f:f32,1,48000 -i:"${tmp}/audio3.wav" -chcopy:1,2 \
         -a:2 -f:f32,1,48000 -i:"${tmp}/audio3l.wav" \
         -a:3 -f:f32,1,48000 -i:"${tmp}/audio3r.wav" -chmove:1,2 \
         -a:all -f:f32,2,48000 -o:"${tmp}/audio.wav"
echo "encoding audio"
oggenc -b 192 "${tmp}/audio.wav"
aseconds="$(( $(stat --format=%s "${tmp}/audio.wav") / (4 * 2 * 48000) ))"
echo "audio length is ${aseconds}"

cat "${tmp}/00001"*.ppm | head -n 2 | tail -n 1 | ( read fwidth fheight
echo "video source frames are ${fwidth}x${fheight}"

echo "encoding video"
ls "${tmp}/"*.ppm | tac | xargs cat | "${zoom}" "${fwidth}" "${fheight}" "${count}" "${aseconds}" |
  avconv -f yuv4mpegpipe -i - -i "${tmp}/audio.ogg" -acodec copy -vb 5M -shortest "${tmp}/video.ogv"

))

echo "done!"
ls -1sh "${tmp}/video.ogv"

popd
